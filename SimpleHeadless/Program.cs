﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;

using CefSharp;
using CefSharp.OffScreen;
using CefSharp.Handler;

using CommandLine;

using Nancy;
using Nancy.Extensions;
using Nancy.Hosting.Self;
using Newtonsoft.Json;

namespace SimpleHeadless
{
    class Program
    {
        private static bool keepRunning = true;

        public class Options
        {
            [Option('p', "port", Required = false, HelpText = "Listen port.", Default = 1234)]
            public int Port { get; set; }
        }

        static void Main(string[] args)
        {
            Initialize();
            RealMain(args);
        }

        private static void Initialize()
        {
            Console.CancelKeyPress += delegate (object sender, ConsoleCancelEventArgs e)
            {
                e.Cancel = true;
                keepRunning = false;
            };
        }

        private static void RealMain(string[] args)
        {
            InitializeCEF();

            Options options = new Options();
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed(o => options = o);

            ServeAPIServer(options.Port);
            ShutdownCEF();
        }

        static private void ServeAPIServer(int port)
        {
            var hostConfig = new HostConfiguration
            {
                UrlReservations = new UrlReservations { CreateAutomatically = true }
            };
            var uri = new Uri(String.Format("http://localhost:{0}/", port));

            var host = new NancyHost(hostConfig, uri);
            host.Start();
            Console.WriteLine("Running on {0}", uri.ToString());

            while (keepRunning) { Thread.Sleep(1000); }

            Console.WriteLine("Stopping server");
            host.Stop();
        }

        static private void InitializeCEF()
        {
            //Monitor parent process exit and close subprocesses if parent process exits first
            CefSharpSettings.SubprocessExitIfParentProcessClosed = true;

            var settings = new CefSettings()
            {
                // By default CefSharp will use an in-memory cache, you need to specify a Cache Folder to persist data
                // CachePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CefSharp\\Cache")
            };

            //Perform dependency check to make sure all relevant resources are in our output directory.
            Cef.Initialize(settings, performDependencyCheck: true, browserProcessHandler: null);
        }

        static private void ShutdownCEF()
        {
            Console.WriteLine("Shutting down CEF");
            Cef.Shutdown();
        }
    }

    public class BrowserModule : NancyModule
    {
        private static Dictionary<string, BrowserInstance> instances = new Dictionary<string, BrowserInstance>();

        public BrowserModule() : base("/browser")
        {
            Post["/"] = parameters => NewSession(parameters);
            Delete["/{instance}"] = parameters => DeleteSession(parameters);

            Post["/{instance}/navigate"] = parameters => Navigate(parameters);
            Get["/{instance}/cookies"] = parameters => GetCookies(parameters);
            Get["/{instance}/screenshot"] = parameters => GetScreenshot(parameters);
            Get["/{instance}/html"] = parameters => GetHTML(parameters);
            Get["/{instance}/is-loading"] = parameters => GetIsLoading(parameters);
            Post["/{instance}/run-script"] = parameters => RunScript(parameters);

            Get["/{instance}/frames"] = parameters => GetFrames(parameters);
            Post["/{instance}/frames/{identifier}/run-script"] = parameters => RunScriptAtFrame(parameters);
            Get["/{instance}/frames/{identifier}/html"] = parameters => GetHTMLAtFrame(parameters);
        }

        private Response NewSession(dynamic parameters)
        {
            string content = this.Request.Body.AsString();
            if (content == null || content.Length == 0)
                return Response.AsJson<dynamic>(new { message = "Body is empty" }, HttpStatusCode.BadRequest);

            NewSessionRequest req = JsonConvert.DeserializeObject<NewSessionRequest>(content);

            string uuid = Guid.NewGuid().ToString();
            instances.Add(uuid, new BrowserInstance(req.headers));

            return Response.AsJson<dynamic>(new { instanceID = uuid }, HttpStatusCode.OK);
        }

        private Response DeleteSession(dynamic parameters)
        {
            string uuid = parameters["instance"];
            if (!instances.ContainsKey(uuid))
                return Response.AsJson<dynamic>(new { }, HttpStatusCode.OK);

            BrowserInstance instance = instances[uuid];
            instance.Close();
            instances.Remove(uuid);

            return Response.AsJson<dynamic>(new { }, HttpStatusCode.OK);
        }

        private Response GetCookies(dynamic parameters)
        {
            string uuid = parameters["instance"];
            if (!instances.ContainsKey(uuid))
                return Response.AsJson<dynamic>(new { message = "Not Found" }, HttpStatusCode.NotFound);

            BrowserInstance instance = instances[uuid];
            bool allCookies = this.Request.Query["all"] == "1";
            var cookies = instance.GetCookies(allCookies);
            return Response.AsJson<dynamic>(cookies, HttpStatusCode.OK);
        }

        private Response GetFrames(dynamic parameters)
        {
            string uuid = parameters["instance"];
            if (!instances.ContainsKey(uuid))
                return Response.AsJson<dynamic>(new { message = "Not Found" }, HttpStatusCode.NotFound);

            BrowserInstance instance = instances[uuid];

            var browser = instance.GetBrowser();
            List<dynamic> frames = new List<dynamic>();
            foreach (long i in browser.GetFrameIdentifiers())
            {
                IFrame frame = browser.GetFrame(i);
                frames.Add(new
                {
                    url = frame.Url,
                    name = frame.Name,
                    identifier = frame.Identifier
                });
            }

            return Response.AsJson<dynamic>(frames, HttpStatusCode.OK);
        }

        private Response Navigate(dynamic parameters)
        {
            string content = this.Request.Body.AsString();
            if (content == null || content.Length == 0)
                return Response.AsJson<dynamic>(new { message = "Body is empty" }, HttpStatusCode.BadRequest);

            NavigateRequest req = JsonConvert.DeserializeObject<NavigateRequest>(content);

            if (req == null || req.url == null || req.url.Trim().Length == 0)
                return Response.AsJson<dynamic>(new { message = "QueryParam \"url\" must be defined." }, HttpStatusCode.BadRequest);

            string uuid = parameters["instance"];
            if (!instances.ContainsKey(uuid))
                return Response.AsJson<dynamic>(new { message = "Not Found" }, HttpStatusCode.NotFound);

            BrowserInstance instance = instances[uuid];
            instance.Navigate(req.url);
            return Response.AsJson<dynamic>(new { }, HttpStatusCode.OK);
        }

        private Response GetScreenshot(dynamic parameters)
        {
            string uuid = parameters["instance"];
            if (!instances.ContainsKey(uuid))
                return Response.AsJson<dynamic>(new { message = "Not Found" }, HttpStatusCode.NotFound);

            BrowserInstance instance = instances[uuid];
            var screenshot = instance.Screenshot();
            if (screenshot == null)
                return Response.AsJson<dynamic>(new { message = "Browser not ready no take screenshot" }, HttpStatusCode.BadRequest);

            MemoryStream stream = new MemoryStream();
            screenshot.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
            screenshot.Dispose();

            stream.Seek(0, SeekOrigin.Begin);
            return Response.FromStream(stream, "image/png").WithStatusCode(HttpStatusCode.OK);
        }

        private Response GetHTML(dynamic parameters)
        {
            string uuid = parameters["instance"];
            if (!instances.ContainsKey(uuid))
                return Response.AsJson<dynamic>(new { message = "Not Found" }, HttpStatusCode.NotFound);

            BrowserInstance instance = instances[uuid];
            string content = instance.HTML();
            return Response.AsText(content);
        }

        private Response GetIsLoading(dynamic parameters)
        {
            string uuid = parameters["instance"];
            if (!instances.ContainsKey(uuid))
                return Response.AsJson<dynamic>(new { message = "Not Found" }, HttpStatusCode.NotFound);

            BrowserInstance instance = instances[uuid];
            return Response.AsJson<dynamic>(new { isLoading = instance.IsLoading() }, HttpStatusCode.OK);
        }

        private Response RunScript(dynamic parameters)
        {
            string uuid = parameters["instance"];
            if (!instances.ContainsKey(uuid))
                return Response.AsJson<dynamic>(new { message = "Not Found" }, HttpStatusCode.NotFound);

            BrowserInstance instance = instances[uuid];
            var result = instance.RunScript(this.Request.Body.AsString());
            if (result.Success)
            {
                return Response.AsText(result.Result?.ToString());
            }
            return Response.AsText(result.Message);
        }

        private Response RunScriptAtFrame(dynamic parameters)
        {
            string uuid = parameters["instance"];
            if (!instances.ContainsKey(uuid))
                return Response.AsJson<dynamic>(new { message = "Not Found" }, HttpStatusCode.NotFound);

            BrowserInstance instance = instances[uuid];
            long identifier = parameters["identifier"];
            var result = instance.RunScriptAtFrame(identifier, this.Request.Body.AsString());
            return Response.AsText(result.Success ? result.Result?.ToString() : result.Message);
        }

        private Response GetHTMLAtFrame(dynamic parameters)
        {
            string uuid = parameters["instance"];
            if (!instances.ContainsKey(uuid))
                return Response.AsJson<dynamic>(new { message = "Not Found" }, HttpStatusCode.NotFound);

            BrowserInstance instance = instances[uuid];
            long identifier = parameters["identifier"];
            string content = instance.HTMLAtFrame(identifier);
            return Response.AsText(content);
        }
    }

    internal class BrowserInstance
    {
        private ChromiumWebBrowser browser;

        public BrowserInstance(Dictionary<string, string> headers)
        {
            browser = new ChromiumWebBrowser();
            browser.RequestHandler = new RequestHandler(headers);
            browser.JsDialogHandler = new JsDialogHandler();
        }

        public void Navigate(string url)
        {
            int i = 0;
            while (!browser.IsBrowserInitialized)
            {
                Console.WriteLine("Waiting instance {0} be ready.", url);
                Thread.Sleep(1000);
                i++;
                if (i >= 5)
                {
                    return;
                }
            }

            browser.Load(url);
        }

        public System.Drawing.Bitmap Screenshot()
        {
            return browser.ScreenshotOrNull();
        }

        public List<Cookie> GetCookies(bool all = true)
        {
            if (all)
            {
                var allCookies = browser.GetCookieManager().VisitAllCookiesAsync();
                return allCookies.Result;
            }
            var urlCookies = browser.GetCookieManager().VisitUrlCookiesAsync(browser.Address, true);
            return urlCookies.Result;
        }

        public IBrowser GetBrowser()
        {
            return browser.GetBrowser();
        }

        public string HTML()
        {
            var task = browser.GetSourceAsync();
            task.Wait();
            return task.Result;
        }

        public string HTMLAtFrame(long identifier)
        {
            var task = browser.GetBrowser().GetFrame(identifier).GetSourceAsync();
            task.Wait();
            return task.Result;
        }

        public bool IsLoading()
        {
            return browser.IsLoading;
        }

        public JavascriptResponse RunScript(string script)
        {
            var task = browser.EvaluateScriptAsync(script);
            task.Wait();
            return task.Result;
        }

        public JavascriptResponse RunScriptAtFrame(long identifier, string script)
        {
            var frame = browser.GetBrowser().GetFrame(identifier);
            var task = frame.EvaluateScriptAsync(script);
            task.Wait();
            return task.Result;
        }

        public void Close()
        {
            browser.Dispose();
        }
    }

    internal class JsDialogHandler : IJsDialogHandler
    {
        public bool OnBeforeUnloadDialog(IWebBrowser chromiumWebBrowser, IBrowser browser, string messageText, bool isReload, IJsDialogCallback callback)
        {
            return true;
        }

        public void OnDialogClosed(IWebBrowser chromiumWebBrowser, IBrowser browser)
        {
            return;
        }

        public bool OnJSDialog(IWebBrowser chromiumWebBrowser, IBrowser browser, string originUrl, CefJsDialogType dialogType, string messageText, string defaultPromptText, IJsDialogCallback callback, ref bool suppressMessage)
        {
            suppressMessage = true;
            return false;
        }

        public void OnResetDialogState(IWebBrowser chromiumWebBrowser, IBrowser browser)
        {
            return;
        }
    }

    internal class RequestHandler : DefaultRequestHandler
    {
        private Dictionary<string, string> customHeaders;

        public RequestHandler(Dictionary<string, string> headers)
        {
            customHeaders = headers;
        }

        public override CefReturnValue OnBeforeResourceLoad(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, IRequestCallback callback)
        {
            if (customHeaders == null || customHeaders.Count == 0)
                return CefReturnValue.Continue;

            var headers = request.Headers;
            foreach (KeyValuePair<string, string> pair in customHeaders)
            {
                headers[pair.Key] = pair.Value;
            }
            request.Headers = headers;

            return CefReturnValue.Continue;
        }
    }

#pragma warning disable 0649
    internal class NavigateRequest
    {
        public string url;
    }

    internal class NewSessionRequest
    {
        public Dictionary<string, string> headers;
    }
#pragma warning restore 0649
}
